# RSMP JS Bridge

A bridge service written in Go. Designed to be run as a child process in a project that doesn't have named pipes like JS. Attaches to a named pipe and bridges that named pipe to the parent process over a standard pipe.