package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/redshift-development/rsap-go/pipe"
)

func main() {
	// Get Pipe Name from CLI flags
	var pipeName string
	flag.StringVar(&pipeName, "name", "redshift-pipe", "Name of pipe")
	flag.Parse()

	// Set up signal waiting
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	// Pipe bridge
	named, err := pipe.DialNamedPipe(pipeName)
	if err != nil {
		panic("failed to dial pipe: " + err.Error())
	}
	std := pipe.NewStdPipe(os.Stdin, os.Stdout, os.Stderr)
	go func() {
		for {
			out, ok := <-named.Out()
			if !ok {
				fmt.Println("output failed")
				break
			}
			err := std.Out(out)
			if err != nil {
				fmt.Println("named output failed")
			}
		}
	}()
	go func() {
		for {
			in, ok := <-std.In()
			if !ok {
				fmt.Println("input failed")
				break
			}
			err := named.In(in)
			if err != nil {
				fmt.Println("named input failed")
			}
		}
	}()

	// Wait for a shutdown signal
	<-sigs
}
